package com.classes;

import java.util.Objects;

public class Monitor {
    static int n =0;
    private int id;
    private String numeroDeSerie;
    private String marca;
    private String modelo;
    private String color;
    private double tamano;
    public Monitor(String a,String b, String c,String d,double e){
       this.numeroDeSerie = a;
       this.marca = b;
       this.modelo = c;
       this.color = d;
       this.tamano = e;
       this.id = n;
       n++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumeroDeSerie() {
        return numeroDeSerie;
    }

    public void setNumeroDeSerie(String numeroDeSerie) {
        this.numeroDeSerie = numeroDeSerie;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getTamano() {
        return tamano;
    }

    public void setTamano(double tamano) {
        this.tamano = tamano;
    }
    
    @Override
    public String toString(){
       return this.id+","+this.numeroDeSerie+","+this.marca+","+
               this.modelo+","+this.color+","+this.tamano;
    }


    @Override
    public boolean equals(Object other){
         Monitor monitor = (Monitor)other;
         return monitor.getNumeroDeSerie().equals(this.numeroDeSerie) &&
         monitor.getModelo().equals(this.modelo) &&
                 monitor.getMarca().equals(this.marca);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.id;
        hash = 29 * hash + Objects.hashCode(this.numeroDeSerie);
        hash = 29 * hash + Objects.hashCode(this.marca);
        hash = 29 * hash + Objects.hashCode(this.modelo);
        hash = 29 * hash + Objects.hashCode(this.color);
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.tamano) ^ (Double.doubleToLongBits(this.tamano) >>> 32));
        return hash;
    }
}